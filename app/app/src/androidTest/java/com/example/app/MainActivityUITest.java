package com.example.app;

import androidx.test.espresso.intent.Intents;
import androidx.test.filters.SmallTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import com.example.app.controllers.ImageListActivity;
import com.example.app.controllers.MainActivity;
import com.robotium.solo.Solo;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.intent.Intents.intended;
import static androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static junit.framework.TestCase.assertTrue;

@SmallTest
@RunWith(AndroidJUnit4.class)
public class MainActivityUITest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void searchUsernameEmpty() {
        Solo solo = new Solo(getInstrumentation(), mActivityTestRule.getActivity());
        getInstrumentation().waitForIdleSync();

        String emptyString = "";
        onView(withId(R.id.etUsername)).perform(typeText(emptyString), closeSoftKeyboard());

        onView(withId(R.id.btnFindUser)).perform(click());

        assertTrue("No alert dialog found!", solo.searchText("App Messenger"));
    }

    @Test
    public void searchUsernameInvalid() {
        Solo solo = new Solo(getInstrumentation(), mActivityTestRule.getActivity());
        getInstrumentation().waitForIdleSync();

        String invalidUsername = "amor1111";
        onView(withId(R.id.etUsername)).perform(typeText(invalidUsername), closeSoftKeyboard());

        onView(withId(R.id.btnFindUser)).perform(click());

        assertTrue("No alert dialog found!", solo.searchText("App Messenger"));
    }

    @Test
    public void searchUsernameValid() {
        Intents.init();
        getInstrumentation().waitForIdleSync();

        String validUsername = "eyetwist";
        onView(withId(R.id.etUsername)).perform(typeText(validUsername), closeSoftKeyboard());

        onView(withId(R.id.btnFindUser)).perform(click());

        intended(hasComponent(ImageListActivity.class.getName()));
    }
}
