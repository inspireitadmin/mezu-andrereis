package com.example.app.mocks;

import com.example.app.models.gallery.Gallery;
import com.example.app.models.gallery.GalleryImage;
import com.example.app.models.gallery.GalleryResponse;
import com.example.app.models.image.Image;
import com.example.app.models.image.ImageResponse;
import com.example.app.models.image.ImageSize;
import com.example.app.models.user.User;
import com.example.app.models.user.UserResponse;
import com.example.app.models.user.Username;
import com.example.app.services.flickr.IFlickrService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.mock.BehaviorDelegate;

public class MockIFlickrService implements IFlickrService {

    private final BehaviorDelegate<IFlickrService> delegate;

    public MockIFlickrService(BehaviorDelegate<IFlickrService> service) {
        this.delegate = service;
    }

    @Override
    public Call<UserResponse> validateUsername(String username) {
        Username name = new Username("eyetwist");
        User user = new User("49191827@N00", "49191827@N00", name);
        UserResponse response = new UserResponse(user, "ok");
        return delegate.returningResponse(response).validateUsername(username);
    }

    @Override
    public Call<GalleryResponse> getImagesByUserId(String userId) {
        final GalleryImage image1 = new GalleryImage(1, "test1");
        final GalleryImage image2 = new GalleryImage(2, "test2");
        final GalleryImage image3 = new GalleryImage(3, "test3");
        final GalleryImage image4 = new GalleryImage(4, "test4");
        final GalleryImage image5 = new GalleryImage(5, "test5");
        final GalleryImage image6 = new GalleryImage(6, "test6");

        List<GalleryImage> images = new ArrayList<GalleryImage>() {
            { add(image1); add(image2); add(image3); add(image4); add(image5); add(image6); }
        };

        Gallery gallery = new Gallery(images);
        GalleryResponse response = new GalleryResponse(gallery, "ok");
        return delegate.returningResponse(response).getImagesByUserId(userId);
    }

    @Override
    public Call<ImageResponse> getImageSizes(long photoId) {
        final ImageSize image1 = new ImageSize("Original", "https://images.unsplash.com/photo-1534188753412-3e26d0d618d6?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1000&q=80", 1024, 1024);

        List<ImageSize> images = new ArrayList<ImageSize>() {
            { add(image1); }
        };

        Image image = new Image(images);
        ImageResponse response = new ImageResponse(image, "ok");
        return delegate.returningResponse(response).getImageSizes(photoId);
    }
}
