package com.example.app;

import android.content.Context;
import android.content.Intent;

import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.filters.SmallTest;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import com.example.app.controllers.ImageListActivity;
import com.example.app.mocks.MockIFlickrService;
import com.example.app.models.gallery.GalleryResponse;
import com.example.app.models.user.User;
import com.example.app.models.user.Username;
import com.example.app.services.flickr.IFlickrService;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.mock.BehaviorDelegate;
import retrofit2.mock.MockRetrofit;
import retrofit2.mock.NetworkBehavior;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;

@SmallTest
@RunWith(AndroidJUnit4.class)
public class ImageListActivityUITest {

    @Rule
    public ActivityTestRule<ImageListActivity> mActivityTestRule =
        new ActivityTestRule<ImageListActivity>(ImageListActivity.class) {
            @Override
            protected Intent getActivityIntent() {
                Context targetContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
                Intent intent = new Intent(targetContext, ImageListActivity.class);
                User user = new User("49191827@N00", "49191827@N00", new Username("eyetwist"));
                intent.putExtra("user_key", user);
                return intent;
            }
        };

    private MockRetrofit mockRetrofit;
    private Retrofit retrofit;

    @Before
    public void setup(){
        retrofit = new Retrofit.Builder().baseUrl("https://www.flickr.com")
                .client(new OkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        NetworkBehavior behavior = NetworkBehavior.create();

        mockRetrofit = new MockRetrofit.Builder(retrofit)
                .networkBehavior(behavior)
                .build();
    }

    @Test
    public void listUserGallery() throws IOException {
        getInstrumentation().waitForIdleSync();

        BehaviorDelegate<IFlickrService> delegate = mockRetrofit.create(IFlickrService.class);
        IFlickrService mockedService = new MockIFlickrService(delegate);

        Call<GalleryResponse> call = mockedService.getImagesByUserId("49191827@N00");
        call.execute();

        onView(withId(R.id.rvImageList)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
    }
}

