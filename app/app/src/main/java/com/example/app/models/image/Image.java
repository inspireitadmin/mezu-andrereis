package com.example.app.models.image;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Image implements Parcelable {

    @SerializedName("size")
    private List<ImageSize> sizes;

    public List<ImageSize> getSizes() {
        return sizes;
    }
    public void setSizes(List<ImageSize> sizes) {
        this.sizes = sizes;
    }

    public Image() {

    }

    public Image(List<ImageSize> sizes) {
        this.sizes = sizes;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.sizes);
    }

    protected Image(Parcel in) {
        this.sizes = in.createTypedArrayList(ImageSize.CREATOR);
    }

    public static final Parcelable.Creator<Image> CREATOR = new Parcelable.Creator<Image>() {
        @Override
        public Image createFromParcel(Parcel source) {
            return new Image(source);
        }

        @Override
        public Image[] newArray(int size) {
            return new Image[size];
        }
    };
}
