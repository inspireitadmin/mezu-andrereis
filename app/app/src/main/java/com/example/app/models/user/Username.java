package com.example.app.models.user;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Username implements Parcelable {

    @SerializedName("_content")
    private String name;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public Username() {

    }

    public Username(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
    }

    protected Username(Parcel in) {
        this.name = in.readString();
    }

    public static final Parcelable.Creator<Username> CREATOR = new Parcelable.Creator<Username>() {
        @Override
        public Username createFromParcel(Parcel source) {
            return new Username(source);
        }

        @Override
        public Username[] newArray(int size) {
            return new Username[size];
        }
    };
}
