package com.example.app.models.image;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class ImageResponse implements Parcelable {

    @SerializedName("sizes")
    private Image image;

    @SerializedName("stat")
    private String status;

    public Image getImage() {
        return image;
    }
    public void setImage(Image image) {
        this.image = image;
    }

    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    public ImageResponse() {

    }

    public ImageResponse(Image image, String status) {
        this.image = image;
        this.status = status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.image, flags);
        dest.writeString(this.status);
    }

    protected ImageResponse(Parcel in) {
        this.image = in.readParcelable(Image.class.getClassLoader());
        this.status = in.readString();
    }

    public static final Parcelable.Creator<ImageResponse> CREATOR = new Parcelable.Creator<ImageResponse>() {
        @Override
        public ImageResponse createFromParcel(Parcel source) {
            return new ImageResponse(source);
        }

        @Override
        public ImageResponse[] newArray(int size) {
            return new ImageResponse[size];
        }
    };
}
