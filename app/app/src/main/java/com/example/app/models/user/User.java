package com.example.app.models.user;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class User implements Parcelable {

    @SerializedName("id")
    private String id;

    @SerializedName("nsid")
    private String nsid;

    @SerializedName("username")
    private Username username;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getNsid() {
        return nsid;
    }
    public void setNsid(String nsid) {
        this.nsid = nsid;
    }

    public Username getUsername() {
        return username;
    }
    public void setUsername(Username username) {
        this.username = username;
    }

    public User() {

    }

    public User(String id, String nsid, Username username) {
        this.id = id;
        this.nsid = nsid;
        this.username = username;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.nsid);
        dest.writeParcelable(this.username, flags);
    }

    protected User(Parcel in) {
        this.id = in.readString();
        this.nsid = in.readString();
        this.username = in.readParcelable(Username.class.getClassLoader());
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
