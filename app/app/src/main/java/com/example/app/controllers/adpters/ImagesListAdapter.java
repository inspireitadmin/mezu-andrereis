package com.example.app.controllers.adpters;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.app.R;
import com.example.app.models.gallery.GalleryImage;

import java.util.List;

public class ImagesListAdapter extends RecyclerView.Adapter<ImageViewHolder> {

    private Context mContext;

    private List<GalleryImage> mImages;

    private IImageListItemOnClickListener mListener;

    public ImagesListAdapter(Context context, List<GalleryImage> images, IImageListItemOnClickListener listener) {
        mContext = context;
        mImages = images;
        mListener = listener;
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ImageViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image_list, parent, false));
    }

    @Override
    public void onBindViewHolder(ImageViewHolder holder, int position) {
        holder.name.setText(mImages.get(position).getTitle());

        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.mipmap.ic_launcher_round)
                .error(R.mipmap.ic_launcher_round);

        Uri uri = Uri.parse("https://image.shutterstock.com/image-vector/lion-icon-illustration-isolated-vector-260nw-560814217.jpg");
        Glide.with(mContext)
                .load(uri)
                .apply(options)
                .into(holder.image);

        holder.setOnClickListener(mListener, mImages.get(position));
    }

    @Override
    public int getItemCount() {
        return mImages != null ? mImages.size() : 0;
    }

    public interface IImageListItemOnClickListener {
        void onClick(GalleryImage image);
    }
}

class ImageViewHolder extends RecyclerView.ViewHolder {

    public LinearLayout root;
    public TextView name;
    public ImageView image;

    public ImageViewHolder(View itemView) {
        super(itemView);
        root = itemView.findViewById(R.id.llRoot);
        name = itemView.findViewById(R.id.tvName);
        image = itemView.findViewById(R.id.ivImage);
    }

    public void setOnClickListener(final ImagesListAdapter.IImageListItemOnClickListener listener, final GalleryImage image) {
        root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onClick(image);
            }
        });
    }
}