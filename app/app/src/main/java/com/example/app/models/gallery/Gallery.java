package com.example.app.models.gallery;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Gallery implements Parcelable {

    @SerializedName("photo")
    private List<GalleryImage> photos;

    public List<GalleryImage> getPhotos() {
        return photos;
    }
    public void setPhotos(List<GalleryImage> photos) {
        this.photos = photos;
    }

    public Gallery() {

    }

    public Gallery(List<GalleryImage> photos) {
        this.photos = photos;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.photos);
    }

    protected Gallery(Parcel in) {
        this.photos = in.createTypedArrayList(GalleryImage.CREATOR);
    }

    public static final Creator<Gallery> CREATOR = new Creator<Gallery>() {
        @Override
        public Gallery createFromParcel(Parcel source) {
            return new Gallery(source);
        }

        @Override
        public Gallery[] newArray(int size) {
            return new Gallery[size];
        }
    };
}
