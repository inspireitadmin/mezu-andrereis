package com.example.app.controllers;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;

import com.example.app.helpers.Messenger;

import com.example.app.R;
import com.example.app.controllers.adpters.ImagesListAdapter;
import com.example.app.models.gallery.Gallery;
import com.example.app.models.gallery.GalleryImage;
import com.example.app.models.gallery.GalleryResponse;
import com.example.app.models.user.User;
import com.example.app.services.RetrofitClientInstance;
import com.example.app.services.flickr.IFlickrService;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ImageListActivity extends AppCompatActivity implements ImagesListAdapter.IImageListItemOnClickListener {

    private IFlickrService mService;

    private RecyclerView mImageListRecyclerView;

    private ProgressBar mLoadingProgressBar;

    private User mUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initData();
        initView();

        mService = RetrofitClientInstance.getRetrofitInstance().create(IFlickrService.class);

        loadUserImages();
    }

    private void initData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) mUser = extras.getParcelable("user_key");
        if (mUser == null) onBackPressed();
    }

    private void initView() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_image_list);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(mUser.getUsername().getName());
        setSupportActionBar(toolbar);

        mImageListRecyclerView = findViewById(R.id.rvImageList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mImageListRecyclerView.setLayoutManager(layoutManager);

        mLoadingProgressBar = findViewById(R.id.pbLoading);
    }

    private void loadUserImages() {
        setViewLoading(true);
        Call<GalleryResponse> call =  mService.getImagesByUserId(mUser.getId());
        call.enqueue(new Callback<GalleryResponse>() {
            @Override
            public void onResponse(Call<GalleryResponse> call, Response<GalleryResponse> response) {
                if (response.body() == null || Objects.equals(response.body().getStatus(), "fail")) handleLoadUserImagesError();
                else handleLoadUserImagesSuccess(response.body());
            }

            @Override
            public void onFailure(Call<GalleryResponse> call, Throwable t) {
                handleLoadUserImagesError();
            }
        });
    }

    private void setViewLoading(boolean isLoading) {
        mLoadingProgressBar.setVisibility(isLoading ? View.VISIBLE : View.INVISIBLE);
        mImageListRecyclerView.setVisibility(isLoading ? View.INVISIBLE : View.VISIBLE);
    }

    private void handleLoadUserImagesSuccess(GalleryResponse response) {
        setViewLoading(false);
        Gallery gallery = response.getGallery();
        ImagesListAdapter adapter = new ImagesListAdapter(this, gallery.getPhotos(), this);
        mImageListRecyclerView.setAdapter(adapter);
    }

    private void handleLoadUserImagesError() {
        setViewLoading(false);
        new Messenger(this)
            .setTitle("App Messenger")
            .setMessage("Unable to retrieve this user's gallery photos, try again later...")
            .show();
    }

    @Override
    public void onClick(GalleryImage image) {
        if (image == null) return;

        Intent intent = new Intent(this, ImageDetailActivity.class);
        intent.putExtra("image_key", image);
        startActivity(intent);
    }
}
