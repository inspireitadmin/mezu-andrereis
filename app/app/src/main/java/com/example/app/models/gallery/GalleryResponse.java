package com.example.app.models.gallery;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class GalleryResponse implements Parcelable {

    @SerializedName("photos")
    private Gallery gallery;

    @SerializedName("stat")
    private String status;

    public Gallery getGallery() {
        return gallery;
    }
    public void setGallery(Gallery gallery) {
        this.gallery = gallery;
    }

    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    public GalleryResponse() {

    }

    public GalleryResponse(Gallery gallery, String status) {
        this.gallery = gallery;
        this.status = status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.gallery, flags);
        dest.writeString(this.status);
    }

    protected GalleryResponse(Parcel in) {
        this.gallery = in.readParcelable(Gallery.class.getClassLoader());
        this.status = in.readString();
    }

    public static final Creator<GalleryResponse> CREATOR = new Creator<GalleryResponse>() {
        @Override
        public GalleryResponse createFromParcel(Parcel source) {
            return new GalleryResponse(source);
        }

        @Override
        public GalleryResponse[] newArray(int size) {
            return new GalleryResponse[size];
        }
    };
}
