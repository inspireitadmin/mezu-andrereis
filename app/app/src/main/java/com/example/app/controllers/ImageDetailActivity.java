package com.example.app.controllers;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.app.R;
import com.example.app.helpers.Messenger;
import com.example.app.models.gallery.GalleryImage;
import com.example.app.models.image.Image;
import com.example.app.models.image.ImageResponse;
import com.example.app.models.image.ImageSize;
import com.example.app.services.RetrofitClientInstance;
import com.example.app.services.flickr.IFlickrService;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ImageDetailActivity extends AppCompatActivity {

    private IFlickrService mService;

    private ScrollView mRootScrollView;

    private ProgressBar mLoadingProgressBar;

    private GalleryImage mImage;

    private ImageView mSourceImageView;

    private TextView mTitleTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initData();
        initView();

        mService = RetrofitClientInstance.getRetrofitInstance().create(IFlickrService.class);

        loadImageDetails();
    }

    private void initData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) mImage = extras.getParcelable("image_key");
        if (mImage == null) onBackPressed();
    }

    private void initView() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_image_detail);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Image Details");
        setSupportActionBar(toolbar);

        mRootScrollView = findViewById(R.id.svRoot);

        mLoadingProgressBar = findViewById(R.id.pbLoading);

        mSourceImageView = findViewById(R.id.ivSource);

        mTitleTextView = findViewById(R.id.tvTitle);
        mTitleTextView.setText(mImage.getTitle());
    }

    private void loadImageDetails() {
        setViewLoading(true);
        Call<ImageResponse> call = mService.getImageSizes(mImage.getId());
        call.enqueue(new Callback<ImageResponse>() {
            @Override
            public void onResponse(Call<ImageResponse> call, Response<ImageResponse> response) {
                if (response.body() == null || Objects.equals(response.body().getStatus(), "fail")) handleLoadImageDetailsError();
                else handleLoadImageDetailsSuccess(response.body());
            }

            @Override
            public void onFailure(Call<ImageResponse> call, Throwable t) {
                handleLoadImageDetailsError();
            }
        });
    }

    private void setViewLoading(boolean isLoading) {
        mLoadingProgressBar.setVisibility(isLoading ? View.VISIBLE : View.INVISIBLE);
        mRootScrollView.setVisibility(isLoading ? View.INVISIBLE : View.VISIBLE);
    }

    private void handleLoadImageDetailsSuccess(ImageResponse response) {
        ImageSize image = getCorrectSize(response.getImage());
        setViewLoading(false);

        if (image == null) {
            new Messenger(this)
                .setTitle("App Messenger")
                .setMessage("Could not load the image itself, try again!")
                .show();

            return;
        }

        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.mipmap.ic_launcher_round)
                .error(R.mipmap.ic_launcher_round);

        Uri uri = Uri.parse(image.getSource());
        Glide.with(this)
                .load(uri)
                .apply(options)
                .into(mSourceImageView);
    }

    private void handleLoadImageDetailsError() {
        setViewLoading(false);
        new Messenger(this)
            .setTitle("App Messenger")
            .setMessage("Unable to load image, please try again!")
            .show();
    }

    private ImageSize getCorrectSize(Image image) {
        if (image.getSizes().size() == 0)
            return null;

        for (ImageSize size: image.getSizes())
            if (size.getLabel().equals("Original")) return size;

        for (ImageSize size: image.getSizes())
            if (size.getLabel().equals("Large")) return size;

        for (ImageSize size: image.getSizes())
            if (size.getLabel().equals("Medium")) return size;

        return image.getSizes().get(0);
    }
}
