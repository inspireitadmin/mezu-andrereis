package com.example.app.services.flickr;

import com.example.app.models.gallery.GalleryResponse;
import com.example.app.models.image.ImageResponse;
import com.example.app.models.user.UserResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface IFlickrService {

    @GET("/services/rest/?method=flickr.people.findByUsername&api_key=06e8247c326832c9d8436b177cf6726e&format=json&nojsoncallback=1")
    Call<UserResponse> validateUsername(@Query("username") String username);

    @GET("/services/rest/?method=flickr.people.getPublicPhotos&api_key=06e8247c326832c9d8436b177cf6726e&format=json&nojsoncallback=1")
    Call<GalleryResponse> getImagesByUserId(@Query("user_id") String userId);

    @GET("/services/rest/?method=flickr.photos.getSizes&api_key=06e8247c326832c9d8436b177cf6726e&format=json&nojsoncallback=1")
    Call<ImageResponse> getImageSizes(@Query("photo_id") long photoId);
}
