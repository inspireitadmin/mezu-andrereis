package com.example.app.controllers;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.app.R;
import com.example.app.helpers.Messenger;
import com.example.app.models.user.User;
import com.example.app.models.user.UserResponse;
import com.example.app.services.RetrofitClientInstance;
import com.example.app.services.flickr.IFlickrService;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private IFlickrService mService;

    private Button mFindUserButton;

    private EditText mUsernameEditText;

    private ProgressBar mLoadingProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();

        mService = RetrofitClientInstance.getRetrofitInstance().create(IFlickrService.class);
    }

    private void initView() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mUsernameEditText = findViewById(R.id.etUsername);

        mFindUserButton = findViewById(R.id.btnFindUser);
        mFindUserButton.setOnClickListener(this);

        mLoadingProgressBar = findViewById(R.id.pbLoading);
    }

    @Override
    public void onClick(View view) {
        int viewId = view.getId();

        if (viewId == R.id.btnFindUser) handleFindUserButtonClick();
    }

    private void handleFindUserButtonClick() {
        String username = mUsernameEditText.getText().toString();

        if (username.isEmpty()) {
            new Messenger(this)
                .setTitle("App Messenger")
                .setMessage("Username field is required to search for a user here!")
                .show();
        } else {
            setViewLoading(true);
            Call<UserResponse> call = mService.validateUsername(username);
            call.enqueue(new Callback<UserResponse>() {
                @Override
                public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                    if (response.body() == null || Objects.equals(response.body().getStatus(), "fail")) handleValidateUsernameError();
                    else handleValidateUsernameSuccess(response.body());
                }

                @Override
                public void onFailure(Call<UserResponse> call, Throwable t) {
                    handleValidateUsernameError();
                }
            });
        }
    }

    private void setViewLoading(boolean isLoading) {
        mLoadingProgressBar.setVisibility(isLoading ? View.VISIBLE : View.INVISIBLE);
        mFindUserButton.setOnClickListener(isLoading ? null : this);
        mUsernameEditText.setEnabled(!isLoading);
    }

    private void handleValidateUsernameSuccess(UserResponse response) {
        setViewLoading(false);
        Intent intent = new Intent(this, ImageListActivity.class);
        User user = response.getUser();
        intent.putExtra("user_key", user);
        startActivity(intent);
    }

    private void handleValidateUsernameError() {
        setViewLoading(false);
        new Messenger(this)
            .setTitle("App Messenger")
            .setMessage("This user does not exist, try another!")
            .show();
    }
}