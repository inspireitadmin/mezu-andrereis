package com.example.app.models.image;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class ImageSize implements Parcelable {

    @SerializedName("label")
    private String label;

    @SerializedName("source")
    private String source;

    @SerializedName("width")
    private int width;

    @SerializedName("height")
    private int height;

    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }

    public String getSource() {
        return source;
    }
    public void setSource(String source) {
        this.source = source;
    }

    public int getWidth() {
        return width;
    }
    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }
    public void setHeight(int height) {
        this.height = height;
    }

    public ImageSize() {

    }

    public ImageSize(String label, String source, int width, int height) {
        this.label = label;
        this.source = source;
        this.width = width;
        this.height = height;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.label);
        dest.writeString(this.source);
        dest.writeInt(this.width);
        dest.writeInt(this.height);
    }

    protected ImageSize(Parcel in) {
        this.label = in.readString();
        this.source = in.readString();
        this.width = in.readInt();
        this.height = in.readInt();
    }

    public static final Parcelable.Creator<ImageSize> CREATOR = new Parcelable.Creator<ImageSize>() {
        @Override
        public ImageSize createFromParcel(Parcel source) {
            return new ImageSize(source);
        }

        @Override
        public ImageSize[] newArray(int size) {
            return new ImageSize[size];
        }
    };
}
