package com.example.app.helpers;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import com.example.app.R;

public class Messenger extends DialogFragment {

    private Context mContext;

    private String mTitle = "App";
    private String mMessage = "What's your message?";

    public Messenger(Context context) {
        mContext = context;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(mTitle);
        builder.setMessage(mMessage)
            .setNeutralButton(R.string.label_messenger_ok_button, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {

                }
            });

        return builder.create();
    }

    public Messenger setTitle(String title) {
        mTitle = title;
        return this;
    }

    public Messenger setMessage(String message) {
        mMessage = message;
        return this;
    }

    public void show() {
        FragmentManager manager = ((FragmentActivity) mContext).getSupportFragmentManager();
        show(manager, "MESSENGER_TAG");
    }

    public void show(String tag) {
        FragmentManager manager = ((FragmentActivity) mContext).getSupportFragmentManager();
        show(manager, tag);
    }
}
