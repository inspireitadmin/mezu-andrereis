# App
This App is composed of three screens, the main one is for looking up a Flickr username, while the others are to list the user images and the details of the image.


# 3rd Party
**Glide** - I used glide to help list and show the images;
**Retrofit** - I used this library to help making http requests and mapping the responses into models;
**Espresso and Robotium** - I used these to help with the UI tests;

# Structure
This App is divided between **Controllers**, **Services** and **Models**. The Controllers are the activity classes themselves. For the services, since the app is really small, I used only one for the Flickr API requests. The models are what the service returns, in this case a User, a Gallery and the Image itself .

# Test
Added UI tests on the Main and the ImageList activities, also mocked the responses of the service to help the tests.